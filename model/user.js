const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    username: String,
    win: Number,
    loss: Number
})

const User = mongoose.model('User', userSchema);

module.exports = User;