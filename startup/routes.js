const express = require("express");
const user = require("../route/user");

module.exports = function(app) {
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use("/api/user", user);
};
