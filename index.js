const express = require("express");
const cors = require("cors");
const app = express();
app.use(cors());
require("./startup/routes")(app);
require("./startup/db")();


app.listen(3001, () => console.log(`runnin at port 3001`));

