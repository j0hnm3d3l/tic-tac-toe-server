const express = require("express");
const router = express.Router();
const User = require("../model/user");

router.post("/",async (req, res) => {
  let { username, win, loss } = req.body;
  let user =await User.findOne({username: username});
  if(user) {
    user.win +=1;
    user.save();
    res.send(user);
  } else {
    const result = new User({
      username,
      win: 1,
      loss
    });

    result.save();
    res.send(result);
  }
  
});

router.get("/", async (req, res) => {
  let result = await User.find().sort({win: -1});
  res.send(result);
});

module.exports = router;
